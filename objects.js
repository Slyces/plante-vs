var objects = {
    arrosoir: {
        nom: "arrosoir",
        type: "outils",
        états: ["vide", "rempli", "aMoitié"],
        place: 0,
        x: 690,
        y: 410,
        mobile: true,
        image: {
            src: "assets/arrosoir.png",
            dimX: 240,
            dimY: 210,
            clés: 3
        }
    },
    fenetre: {
        nom: "fenetre",
        type: "back",
        états: ["hiver", "printemps", "été", "automne"],
        place: 0,
        x: 610,
        y: 171,
        mobile: false,
        image: {
            src: "assets/fenetre.png",
            dimX: 373,
            dimY: 324,
            clés: 4
        }
    },
    pelle: {
        nom: "transplantoir",
        type: "outils",
        états: ["vide", "rempli"],
        place: 0,
        x: 580,
        y: 440,
        mobile: true,
        image: {
            src: "assets/truelle.png",
            dimX: 130,
            dimY: 140,
            clés: 2
        }
    },
    base: {
        nom: "plan de travail",
        type: "back",
        états: ["neutre"],
        place: 0,
        x: 410,
        y: 580,
        mobile: false,
        image: {
            src: "assets/base.png",
            dimX: 250,
            dimY: 112,
            clés: 1
        }
    },
    pot1: {
        nom: "pot1",
        type: "pots", // back ? 
        états: ["vide", "rempli"],
        place: 0,
        x: 115,
        y: 100,
        mobile: true,
        image: {
            src: "assets/pots.png",
            dimX: 242,
            dimY: 182,
            clés: 2
        }
    },
    pot2: {
        nom: "pot2",
        type: "pots", // back ? 
        états: ["vide", "rempli"],
        place: 0,
        x: 225,
        y: 100,
        mobile: true,
        image: {
            src: "assets/pots.png",
            dimX: 242,
            dimY: 182,
            clés: 2
        }
    },
    pot3: {
        nom: "pot3",
        type: "pots", // back ? 
        états: ["vide", "rempli"],
        place: 0,
        x: 325,
        y: 100,
        mobile: true,
        image: {
            src: "assets/pots.png",
            dimX: 242,
            dimY: 182,
            clés: 2
        }
    },
    calendrier: {
        nom: "calendrier",
        type: "temps",
        états: ["janvier", "fevrier", "mars", "avril", "mai", "juin", "juillet", "aout", "septembre", "octobre", "novembre", "decembre"],
        place: 0,
        x: 350,
        y: 60,
        mobile: false,
        image: {
            src: "assets/calendrier.png",
            dimX: 129, //< 
            dimY: 120,
            clés: 12
        }
    },
    robinet: {
        nom: "robinet",
        type: "back",
        états: ["fermé", "ouvert"],
        place: 0,
        x: 30,
        y: 380,
        mobile: false,
        image: {
            src: "assets/robinet.png",
            dimX: 126,
            dimY: 120,
            clés: 2
        }
    },
    planteC: {
        nom: "citronnier",
        type: "plante",
        états: ["graine", "petitePousse", "pousse", "grandePousse", "arbuste", "fleuri", "mûr"],
        place: 0,
        x: -400,
        y: -390,
        mobile: true,
        image: {
            src: "assets/citron.png",
            dimX: 242,
            dimY: 411,
            clés: 7
        }
    },
    planteR: {
        nom: "rose",
        type: "plante",
        états: ["graine", "petitePousse", "pousse", "grandePousse", "fleuri"],
        place: 0,
        x: -400,
        y: -440,
        mobile: true,
        image: {
            src: "assets/rose.png",
            dimX: 242,
            dimY: 296,
            clés: 5
        }
    },
    planteT: {
        nom: "tournesol",
        type: "plante",
        états: ["graine", "petitePousse", "pousse", "grandePousse", "bourgeon", "fleuri"],
        place: 0,
        x: -400,
        y: -390,
        mobile: true,
        image: {
            src: "assets/tournesol.png",
            dimX: 242,
            dimY: 411,
            clés: 6
        }
    },
    sac: {
        nom: "sac de terre",
        type: "back",
        états: ["plein"],
        place: 0,
        x: 200,
        y: 430,
        mobile: false,
        image: {
            src: "assets/terreau.png",
            dimX: 200,
            dimY: 198,
            clés: 1
        }
    },
    graineC: {
        nom: "graine de citronnier",
        type: "graine",
        états: ["neutre"],
        place: 0,
        x: -330,
        y: -220,
        mobile: true,
        image: {
            src: "assets/grC.png",
            dimX: 37,
            dimY: 28,
            clés: 1
        }
    },
    graineT: {
        nom: "graine de tournesol",
        type: "graine",
        états: ["neutre"],
        place: 0,
        x: -330,
        y: -220,
        mobile: true,
        image: {
            src: "assets/grT.png",
            dimX: 37,
            dimY: 26,
            clés: 1
        }
    },
    graineR: {
        nom: "graine de rose",
        type: "graine",
        états: ["neutre"],
        place: 0,
        x: -330,
        y: -220,
        mobile: true,
        image: {
            src: "assets/grR.png",
            dimX: 37,
            dimY: 30,
            clés: 1
        }
    },
    seedT: {
        nom: "sac de graine tournesol",
        type: "sac", //back?
        états: ["plein"],
        place: 0,
        x: 110,
        y: 220,
        mobile: false,
        image: {
            src: "assets/seedT.png",
            dimX: 80,
            dimY: 95,
            clés: 1
        }
    },
    seedR: {
        nom: " sac de graine de Rose",
        type: "sac",
        états: ["plein"],
        place: 0,
        x: 220,
        y: 220,
        mobile: false,
        image: {
            src: "assets/seedR.png",
            dimX: 80,
            dimY: 95,
            clés: 1
        }
    },
    seedC: {
        nom: " sac de graine de citronnier",
        type: "sac",
        états: ["plein"],
        place: 0,
        x: 330,
        y: 220,
        mobile: false,
        image: {
            src: "assets/seedC.png",
            dimX: 80,
            dimY: 95,
            clés: 1
        }
    },
}
