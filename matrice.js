let format = Phaser.Utils.String.Format

function placer_pot(pot, base) {
    pot.setScale(1);
    pot.x = 400;
    pot.y = 500;
    base.place = 1;
}

function placer_base(plante, fenetre, base) {
    plante.setScale(1);
    plante.x = -(plante.baseX);
    plante.y = -(plante.baseY);
    // une place se libère pour la fenêtre
    fenetre.place -= 1;
    // la place de la place, c'est où elle était (1 pour 0, 2 pour 1, ...)
    fenetre.available_slots[plante.place - 1] = true;
    base.place = 1; // la base est occupée
    plante.place = 0; // la plante est sur la base
}

function placer_fenetre(plante, fenetre, base) {
    // on trouve le premier 'vrai' (place libre)
    let first_available = fenetre.available_slots.findIndex(x => x);
    plante.x = fenetre.x + (370 / 3) * (first_available - 1);
    plante.y = fenetre.y + 50;
    plante.setScale(0.5);
    if (plante.ID == 'planteR') {
        plante.y += 30;
    }
    // la base est libre maintenant
    base.place = 0;
    // une place est prise pour la fenêtre
    fenetre.place += 1;
    // la place qu'on a trouvé est prise
    fenetre.available_slots[first_available] = false;
    // on numérote la plante pour savoir où elle est
    plante.place = first_available + 1; // 0 c'est la base donc +1
}

function remplir_pot(pelle, pot) {
    pelle.état = 'vide';
    pot.état = 'rempli';
    pelle.reset();
}

function planter_graine(graine, pot, game) {
    // la graine disparaît
    graine.x = -(graine.x);
    graine.y = -(graine.y);
    // le pot disparaît
    pot.x = -(pot.x);
    pot.y = -(pot.x);

    // on cherche les plantes qui existent deja

    // 1. l'ID de la plante pour cette graine
    let plante_id = 'plante' + graine.ID.slice(-1);

    // on cherche à compter combien de plante on a déjà
    let count = 0;
    for (let sprite_key in game.sprites) {
        if (sprite_key.includes(plante_id)) {
            count++;
        }
    }
    console.log("count → " + count.toString());

    // On crée la nouvelle plante normalement
    let plante = game.create_sprite(plante_id, objects[plante_id]);
    game.bind_events(plante); // on lui paramètre les 'on_click'

    // on la place correctement
    plante.x = -(plante.x);
    plante.y = -(plante.y);
    /* /!\ MAGIE NOIRE /!\
     * on la nomme pas 'planteX' mais 'planteX.0/1/2' pour pouvoir en avoir
     * plusieurs.
     * 'count', c'est combien on en a. Si on en a 0, on peut utiliser 0.
     * Si on en a 1, son nom nom c'est 0, donc on utilise 1. etc ...
     * On veut être dans le tableau des sprites pour la collision, mais le nom
     * compte pas
     */
    game.sprites[plante_id + '.' + (count + 1).toString()] = plante;
}

function arroser(arrosoir, plante, temps) {
    // on avance l'état de la plante, et on vide l'arrosoir
    let plante_index = plante.ETATS.indexOf(plante.état);
    let arrosoir_index = arrosoir.ETATS.indexOf(arrosoir.état);
    plante.état = plante.ETATS[plante_index + 1];
    arrosoir.état = arrosoir.ETATS[(arrosoir_index + 1) % 3];
    arrosoir.reset();

    let mois_index = temps.ETATS.indexOf(temps.état);
    temps.état = temps.ETATS[(mois_index + 1) % 12];

    // on prépare les prochaines instructions
    let template = instructions[plante.ID + '/' + plante.état + '/' + plante.place];
    return format(template, [plante.NOM, phrase_etat[plante.état]]);
}

var phrase_etat = {
    'graine': 'une graine',
    'petitePousse': 'une petite pousse',
    'pousse': 'une pousse',
    'grandePousse': 'une grande pousse',
    'bourgeon': 'un bourgeon',
    'arbuste': 'un arbuste',
    'fleuri': 'fleuri',
    'mûr': 'mûr',
}

var instructions = {
    // citron
    'planteC/graine/0': "Votre %1 est %2. Arrosez le pour le faire grandir.",
    'planteC/petitePousse/0': "Votre %1 est %2. Arrosez le pour le faire grandir.",
    'planteC/pousse/0': "Votre %1 est %2. Arrosez le pour le faire grandir.",
    'planteC/grandePousse/0': "Votre %1 est %2. Arrosez le pour le faire grandir.",
    'planteC/arbuste/0': "Votre %1 est %2. Arrosez le pour le faire grandir.",
    'planteC/fleuri/0': "Votre %1 est %2. Arrosez le pour le faire grandir.",
    'planteC/mûr/0': "Votre %1 est %2. Félicitations !",
    // rose
    'planteR/graine/0': "Votre %1 est %2. Arrosez la pour la faire grandir.",
    'planteR/petitePousse/0': "Votre %1 est %2. Arrosez la pour la faire grandir.",
    'planteR/pousse/0': "Votre %1 est %2. Arrosez la pour la faire grandir.",
    'planteR/grandePousse/0': "Votre %1 est %2. Arrosez la pour la faire grandir.",
    'planteR/fleuri/0': "Votre %1 est %2e. Félicitation !",
    // tournesol
    'planteT/graine/0': "Votre %1 est %2. Arrosez le pour le faire grandir.",
    'planteT/petitePousse/0': "Votre %1 est %2. Arrosez le pour le faire grandir.",
    'planteT/pousse/0': "Votre %1 est %2. Arrosez le pour le faire grandir.",
    'planteT/grandePousse/0': "Votre %1 est %2. Arrosez le pour le faire grandir.",
    'planteT/bourgeon/0': "Votre %1 est %2. Arrosez le pour le faire grandir.",
    'planteT/fleuri/0': "Votre %1 est %2. Félicitation !",
}

var interactions = {
    'pot1/vide/0': {
        'base/neutre/0': {
            'debut': placer_pot
        }
    },
    'pot2/vide/0': {
        'base/neutre/0': {
            'debut': placer_pot
        }
    },
    'pot3/vide/0': {
        'base/neutre/0': {
            'debut': placer_pot
        }
    },
    'pelle/vide/0': {
        'sac/plein/0': {
            'debut': function(pelle) {
                pelle.état = "rempli";
                pelle.reset();
            }
        }
    },
    'pelle/rempli/0': {
        'pot1/vide/0': {
            'debut': remplir_pot
        },
        'pot2/vide/0': {
            'debut': remplir_pot
        },
        'pot3/vide/0': {
            'debut': remplir_pot
        }
    },
    'robinet/fermé': {
        'self': function(robinet) {
            robinet.état = "ouvert";
        }
    },
    'robinet/ouvert': {
        'self': function(robinet) {
            robinet.état = "fermé";
        }
    },
    'arrosoir/vide/0': {
        'robinet/ouvert/0': {
            'debut': function(arrosoir) {
                arrosoir.état = "rempli";
                arrosoir.reset();
            }
        }
    },
    'graineC/neutre': {
        'pot1/rempli/0': {
            'debut': planter_graine
        },
        'pot2/rempli/0': {
            'debut': planter_graine
        },
        'pot3/rempli/0': {
            'debut': planter_graine
        }
    },
    'graineT/neutre': {
        'pot1/rempli/0': {
            'debut': planter_graine
        },
        'pot2/rempli/0': {
            'debut': planter_graine
        },
        'pot3/rempli/0': {
            'debut': planter_graine
        }
    },
    'graineR/neutre': {
        'pot1/rempli/0': {
            'debut': planter_graine
        },
        'pot2/rempli/0': {
            'debut': planter_graine
        },
        'pot3/rempli/0': {
            'debut': planter_graine
        }
    },
    'arrosoir/rempli/0': {
        'planteC/graine/0': {
            'debut': arroser
        },
        'planteR/graine/0': {
            'debut': arroser
        },
        'planteT/graine/0': {
            'debut': arroser
        },
        'planteC/petitePousse/0': {
            'debut': arroser
        },
        'planteR/petitePousse/0': {
            'debut': arroser
        },
        'planteT/petitePousse/0': {
            'debut': arroser
        },
        'planteC/pousse/0': {
            'debut': arroser
        },
        'planteR/pousse/0': {
            'debut': arroser
        },
        'planteT/pousse/0': {
            'debut': arroser
        },
        'planteC/grandePousse/0': {
            'debut': arroser
        },
        'planteR/grandePousse/0': {
            'debut': arroser
        },
        'planteT/grandePousse/0': {
            'debut': arroser
        },
        'planteT/bourgeon/0': {
            'debut': arroser
        },
        'planteC/arbuste/0': {
            'debut': arroser
        },
        'planteC/fleuri/0': {
            'debut': arroser
        }
    },
    'arrosoir/aMoitié/0': {
        'robinet/ouvert/0': {
            'debut': function(arrosoir) {
                arrosoir.état = "rempli";
                arrosoir.reset();
            }
        },
        'planteC/graine/0': {
            'debut': arroser
        },
        'planteR/graine/0': {
            'debut': arroser
        },
        'planteT/graine/0': {
            'debut': arroser
        },
        'planteC/petitePousse/0': {
            'debut': arroser
        },
        'planteR/petitePousse/0': {
            'debut': arroser
        },
        'planteT/petitePousse/0': {
            'debut': arroser
        },
        'planteC/pousse/0': {
            'debut': arroser
        },
        'planteR/pousse/0': {
            'debut': arroser
        },
        'planteT/pousse/0': {
            'debut': arroser
        },
        'planteC/grandePousse/0': {
            'debut': arroser
        },
        'planteR/grandePousse/0': {
            'debut': arroser
        },
        'planteT/grandePousse/0': {
            'debut': arroser
        },
        'planteT/bourgeon/0': {
            'debut': arroser
        },
        'planteC/arbuste/0': {
            'debut': arroser
        },
        'planteC/fleuri/0': {
            'debut': arroser
        }
    },
}

// Pour les plantes y'a juste trop de combinaisons donc on fait des boucles
for (const key in objects) {
    let entity = objects[key];
    if (entity.type == "plante") {
        // pour les plates, on regarde chaque état possible
        let plante = key;
        for (i = 0; i < entity.états.length; i++) {
            let état = entity.états[i];
            let plante_etat = key + '/' + état;
            // on s'assure que la clé 'planteX/etat/0' est dispo
            (plante_etat + '/0') in interactions || (interactions[plante_etat + '/0'] = {});
            for (j in ["0", "1", "2"]) { // la fenêtre peut être 0/1/2
                // plante -> fenetre
                // la place doit être 0 (on est sur la base) pour aller vers la fenêtre
                let saisons = objects['fenetre'].états;
                for (k = 0; k < saisons.length; k++) {
                    interactions[plante_etat + '/0']['fenetre/' + saisons[k] + '/' + j] = {
                        'debut': placer_fenetre
                    };
                }
            }
            for (j in ["0", "1", "2", "3"]) { // la plante (sur fenêtre) peut être 1/2/3
                // plante -> base
                if (j === "0") {  // *^* mystique *^*
                    continue;
                }
                let source = plante_etat + '/' + j;
                // on s'assure que la clé 'planteX/etat/Y' est dispo
                (source) in interactions || (interactions[source] = {});
                interactions[source]['base/neutre/0'] = {
                    'debut': placer_base
                };
            }
        }
    }
}
