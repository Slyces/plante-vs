var object_keys = Object.keys(objects);

class Game extends Phaser.Scene {

    constructor() {
        super("Game");
        this.object_keys = Object.keys(objects);
        this.sprites = {};
    }

    preload() {
        // Load the background
        this.load.image('body', 'assets/fond.png');

        // Load the sprite for each object
        for (i = 0; i < object_keys.length; i++) {
            let key = object_keys[i];
            this.load.spritesheet(
                key,
                objects[key].image.src, {
                    frameWidth: objects[key].image.dimX,
                    frameHeight: objects[key].image.dimY
                },
                objects[key].image.clés
            )
        }
    }

    // Fonction pour update les sprites
    update_sprite(sprite) {
        let etats = objects[sprite.ID].états;
        let index = etats.indexOf(sprite.état);
        sprite.setFrame(index);
    }

    // Fonction pour trouver les collisions
    on_collision(sprite, callable) {
        let sprite_keys = Object.keys(this.sprites);
        for (i = 0; i < sprite_keys.length; i++) {
            let target = this.sprites[sprite_keys[i]];
            if (this.physics.overlap(sprite, target)) {
                // pour chaque sprite on appelle la fonction
                let success = callable(target);
                // si elle reussit on quitte en disant vrai
                if (success) {
                    return true;
                }
            }
        }
        // si on a pas reussi on retourne faux
        return false;
    }

    create_sprite(id, object) {
        // on met le calendrier de maniere a ce qu'il soit toujours accessible facilement
        let sprite = this.physics.add.sprite(object.x, object.y, id);

        // On initialise les attributs du sprite
        sprite.ID = id;
        sprite.TYPE = object.type;
        sprite.NOM = object.nom;
        sprite.ETATS = object.états;
        sprite.état = object.états[0];
        sprite.BOUGE = object.mobile;
        sprite.baseX = object.x;
        sprite.baseY = object.y;
        sprite.place = object.place;

        sprite.backup = function() {
            sprite.backupx = sprite.x;
            sprite.backupy = sprite.y;
        }
        sprite.reset = function() {
            sprite.x = sprite.backupx;
            sprite.y = sprite.backupy;
        }

        sprite.drag = function(dragX, dragY) {
            sprite.x = dragX;
            sprite.y = dragY;
        }

        sprite.setInteractive();
        this.input.setDraggable(sprite);

        // On s'occupe des profondeurs d'objets
        if (object.type === "back" || object.type === "temps" || id.includes('seed')) {
            sprite.setDepth(0);
        } else if (!sprite.ID.includes('graine')) {
            sprite.setDepth(1);
        } else {
            sprite.setDepth(2);
        }

        if (object.type === "temps") {
            // gestion spécifique du calendrier
            sprite.saison = "debut";
        } else if (sprite.ID == 'fenetre') {
            // gestion spécifique de la fenêtre
            sprite.available_slots = [true, true, true];
            sprite.set_saison = function(temps) {
                let index = temps.ETATS.indexOf(temps.état);
                if (index == 11 || index == 0 || index == 1) {
                    sprite.état = sprite.ETATS[0]; // hiver
                } else if (index == 2 || index == 3 || index == 4) {
                    sprite.état = sprite.ETATS[1]; // printemps
                } else if (index == 5 || index == 6 || index == 7) {
                    sprite.état = sprite.ETATS[2]; // été
                } else if (index == 8 || index == 9 || index == 10) {
                    sprite.état = sprite.ETATS[3]; // automne
                }
            }
        } else if (object.type === "pots") { // on rapetit les pots en haut
            // gestion spécifique des pots
            sprite.setScale(.5);
        }
        return sprite;
    }

    // Fonction pour attacher des events a un sprite
    bind_events(sprite) {
        let temps = this.sprites['calendrier'];

        // on s'occupe des objets qui self-interact
        sprite.on('pointerdown', pointer => {
            let source = sprite.ID + '/' + sprite.état;
            console.log("→ self - " + source);
            try {
                interactions[source]['self'](sprite);
                this.update_sprite(sprite);
            } catch (err) {}
        });

        // la logique des sac de graines est specifique: bien qu'on
        // interagisse avec le sac, c'est la graine qui bouge.
        if (sprite.ID.includes("seed")) {
            let graine_id = 'graine' + sprite.ID.slice(-1);
            let graine_sprite = this.sprites[graine_id];

            // si on commence à drag le sachet de graine,
            // on backup les coordonnées de la graine elle même
            sprite.on('dragstart', pointer => {
                graine_sprite.backup()
            });

            // quand on bouge (le sachet), c'est la graine qui bouge
            // (et donc apparaît)
            sprite.on('drag', function(pointer, dragX, dragY) {
                graine_sprite.drag(dragX, dragY)
            });

            sprite.on('dragend', pointer => {
                let success = this.on_collision(
                    graine_sprite,
                    target => {
                        // source/cible dans la matrice
                        let source = graine_id + '/' + graine_sprite.état;
                        let cible = target.ID + '/' + target.état + '/' + target.place;

                        // on essaye la transition
                        try { // ici c'est de la magie noire pour avoir plusieurs plantes
                            interactions[source][cible][temps.saison](
                                graine_sprite, target, this
                            );
                            // la on met les fonction en fonction de ce que ça renvoie
                            // NE PAS OUBLIER LA GESTION DU CALENDRIER
                        } catch (err) {
                            return false;
                        }
                        return true;
                    });
                if (!success) { // si on a pas touché le pot, la graine repart dans l'espace
                    graine_sprite.reset();
                }
            }, this);
        }


        // on s'occupe des objets qui peuvent bouger
        if (sprite.BOUGE) {
            // on sauvegarde la position de base avant de bouger
            sprite.on('dragstart', function(pointer) {
                sprite.backup()
            });

            // le mouvement
            sprite.on('drag', function(pointer, dragX, dragY) {
                sprite.setDepth(2);
                sprite.drag(dragX, dragY);
            });

            // a la fin du mouvement
            sprite.on('dragend', pointer => {
                let success = this.on_collision(
                    sprite,
                    target => {
                        let source = sprite.ID + '/' + sprite.état + '/' + sprite.place;
                        let cible = target.ID + '/' + target.état + '/' + target.place;
                        console.log(source + " → " + cible);

                        let fenetre = this.sprites['fenetre'];
                        let fenetre_ou_base = (target.ID === 'fenetre' || target.ID === 'base');
                        try {
                            // pour bouger les plantes, il faut la fenêtre et la base
                            if (sprite.TYPE === 'plante' && fenetre_ou_base) {
                                interactions[source][cible][temps.saison](
                                    sprite, fenetre, this.sprites['base']
                                );
                            } else {
                                // ici c'est les interactions générales, plus de cas particulier
                                let instruction = interactions[source][cible][temps.saison](
                                    sprite, target, temps
                                );
                                // on teste si c'est du texte (au moins 1 caractere)
                                if (instruction && instruction.length > 0) {
                                    this.Btexte.setText(instruction);
                                }
                            }
                        } catch (err) {
                            return false; // la collision a pas marché
                        }
                        // on update source, cible
                        this.update_sprite(sprite);
                        this.update_sprite(target);
                        // on update calendrier, fenêtre
                        fenetre.set_saison(temps);
                        this.update_sprite(fenetre);
                        this.update_sprite(temps);
                        return true; // la collision a marché
                    });
                if (!success) { // si aucune collision marche, on replace le sprite
                    sprite.reset();
                }
                sprite.setDepth(1); //remet le sprit a la bonne profondeur
            }, this);
        }
    }

    // Fonction pour créer la scene
    create() {
        this.add.image(game.config.width / 2, game.config.height / 2, 'body');

        var style = {
            font: "30px maturamt",
            fill: "#000000",
            align: "left",
            wordWrap: {
                width: 310,
                useAdvancedWrap: true
            }
        };
        this.Btexte = this.add.text(810, 400, "", style).setOrigin(0, 1);

        // On crée tous les sprites en premier
        for (let id in objects) {
            if (!id.includes('plante')) { // Les sprite de plantes seront créés à la demande
                let sprite = this.create_sprite(id, objects[id]);
                this.sprites[id] = sprite;
            }
        }

        // On attache les fonctions à chaque objet une fois que les sprites sont finis
        // pour pas essayer d'accéder à un sprite qui existe pas encore
        for (let sprite_id in this.sprites) {
            let sprite = this.sprites[sprite_id];
            this.bind_events(sprite);
        }
    }
}

var config = {
    type: Phaser.AUTO,
    width: 1111,
    height: 600,
    backgroundColor: 'd0d0d0',
    parent: 'canvas',
    scene: Game,
    physics: {
        default: 'arcade',
        arcade: {
            debug: false,
            gravity: 0
        }
    },
    title: 'plante vs rien du tout',
    version: '1.0'
};
var game = new Phaser.Game(config);
